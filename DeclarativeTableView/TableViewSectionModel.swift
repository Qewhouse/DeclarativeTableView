//
//  TableViewSectionModel.swift
//  DeclarativeTableView
//
//  Created by Alexander Altman on 28.05.2023.
//

import UIKit

class TableViewSectionModel {
    var cells: [TableViewCellModel]
    var header: TableViewHeaderModel?
    
    init(cells: [TableViewCellModel], header: TableViewHeaderModel? = nil) {
        self.cells = cells
        self.header = header
    }
}
