//
//  CustomTableViewCell.swift
//  DeclarativeTableView
//
//  Created by Alexander Altman on 28.05.2023.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
    let titleLabel = UILabel()
    let subtitleLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            // настраиваем titleLabel
            titleLabel.font = UIFont.boldSystemFont(ofSize: 14.0)
            titleLabel.textColor = UIColor.black
            contentView.addSubview(titleLabel)
            
            // настраиваем subtitleLabel
            subtitleLabel.font = UIFont.systemFont(ofSize: 12.0)
            subtitleLabel.textColor = UIColor.gray
            contentView.addSubview(subtitleLabel)
            
            // добавляем autolayout constraints
            titleLabel.translatesAutoresizingMaskIntoConstraints = false
            subtitleLabel.translatesAutoresizingMaskIntoConstraints = false
            
            NSLayoutConstraint.activate([
                titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16.0),
                titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8.0),
                titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16.0),
                
                subtitleLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
                subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 4.0),
                subtitleLabel.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
                subtitleLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8.0)
            ])
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
