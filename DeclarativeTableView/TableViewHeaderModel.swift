//
//  TableViewHeaderModel.swift
//  DeclarativeTableView
//
//  Created by Alexander Altman on 28.05.2023.
//

import UIKit

class TableViewHeaderModel {
    var title: String?
    
    init(title: String?) {
        self.title = title
    }
}
