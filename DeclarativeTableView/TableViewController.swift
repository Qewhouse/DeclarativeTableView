//
//  TableViewController.swift
//  DeclarativeTableView
//
//  Created by Alexander Altman on 25.05.2023.
//

import UIKit

class TableViewController: UIViewController {
    
    private let tableView = UITableView()
    private var tableViewBuilder: TableViewBuilder!
    
    private var data = ["Adam", "John", "Miriam", "Harry", "Ron", "Hermiona", "Wisly", "Dumbledore", "Bilbo", "Frodo", "Tom", "Igor", "Kate", "Dad", "Mom", "Grandpa", "Grandma", "Wife", "Alex", "Anthony", "July"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .cyan
        
        tableView.frame = view.bounds
        view.addSubview(tableView)
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "default")
        tableView.register(CustomTableViewCell.self, forCellReuseIdentifier: "custom")
        
        tableViewBuilder = TableViewBuilder(tableView: tableView)
        
        let cellModel1 = TableViewCellModel(identifier: "default")
        cellModel1.onFill = { cell in
            cell.textLabel?.text = "Default cell"
            cell.backgroundColor = .green
        }
        cellModel1.onSelect = {
            print("Default cell tapped")
            self.tableView.backgroundColor = .brown
        }
        
        
        let cellModel2 = TableViewCellModel(identifier: "custom")
        cellModel2.onFill = { cell in
            if let customCell = cell as? CustomTableViewCell {
                customCell.titleLabel.text = "Custom cell"
                customCell.subtitleLabel.text = "Subtitle"
                customCell.backgroundColor = .yellow
            }
        }
        cellModel2.onSelect = {
            print("Custom cell tapped")
            self.tableView.backgroundColor = .cyan
        }
        
        let sectionModel = TableViewSectionModel(cells: [cellModel1, cellModel2])
        sectionModel.header = TableViewHeaderModel(title: "Section 1")
        
        tableViewBuilder.sections = [sectionModel]
    }
}


