//
//  TableViewCellModel.swift
//  DeclarativeTableView
//
//  Created by Alexander Altman on 28.05.2023.
//

import UIKit

class TableViewCellModel {
    let identifier: String
    var onFill: ((UITableViewCell) -> Void)?
    var onSelect: (() -> Void)?
    
    init(identifier: String) {
        self.identifier = identifier
    }
}
